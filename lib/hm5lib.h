#ifndef HM5LIB_H
#define HM5LIB_H

#include <vector>
#include <algorithm>
#include <stdexcept>

int GetProductsAmount(std::vector<int>& prices, int amountOfMoney);
bool IsLeapYear(int year);

#endif
