#include "hm5lib.h"

bool IsLeapYear(int year)
{
    if (year == 0)
        throw new std::invalid_argument("Year zero does not exist!");
    return (year % 4 == 0) ?
        (year % 100 != 0 || year % 400 == 0) : false;
}
