#include "hm5lib.h"
#include <gtest/gtest.h>

TEST(IsLeapYearTest, ExceptionOnZeroYear)
{
    ASSERT_ANY_THROW(IsLeapYear(0));
}

TEST(IsLeapYearTest, TrueIfDivisibleBy4AndNdBy100)
{
    ASSERT_TRUE(IsLeapYear(4));
}

TEST(IsLeapYearTest, FalseIfDivisibleBy100AndNdBy400)
{
    ASSERT_FALSE(IsLeapYear(100));
}

TEST(IsLeapYearTest, TrueIfDivisibleBy400)
{
    ASSERT_TRUE(IsLeapYear(800));
}

TEST(IsLeapYearTest, FalseIfNotDivisibleBy4)
{
    ASSERT_FALSE(IsLeapYear(811));
}

TEST(IsLeapYearTest, HanldeNegativeYears)
{
    ASSERT_TRUE(IsLeapYear(-4));
    ASSERT_FALSE(IsLeapYear(-100));
    ASSERT_TRUE(IsLeapYear(-800));
    ASSERT_FALSE(IsLeapYear(-811));
}

TEST(IsLeapYearTest, NoThrowOnNumericLimits)
{
    using IntLim = std::numeric_limits<int>;
    ASSERT_NO_THROW(IntLim::max());
    ASSERT_NO_THROW(IntLim::min());
}
