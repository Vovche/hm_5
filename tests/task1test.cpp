#include "hm5lib.h"
#include <gtest/gtest.h>

#include <limits>

using IntLim = std::numeric_limits<int>;

TEST(GetProductsAmountTests, ProofOfConceptTest)
{
    std::vector <int> v{1, 2, 3};
    EXPECT_EQ(3, GetProductsAmount(v, 10));
    EXPECT_EQ(2, GetProductsAmount(v, 3));
    EXPECT_EQ(1, GetProductsAmount(v, 1));
}

TEST(GetProductsAmountTests, HandleZeroSizeVector)
{
    std::vector <int> v;
    EXPECT_EQ(0, GetProductsAmount(v, 1));
}

TEST(GetProductsAmountTests, HandleZeroOrLessAmountOfMoney)
{
    std::vector <int> v{1, 2, 3};
    EXPECT_EQ(0, GetProductsAmount(v, 0));
    EXPECT_EQ(0, GetProductsAmount(v, -5));
}
/*
 * Time to break the function
*/

TEST(GetProductsAmountTests, HandleNegativePrices)
{
    std::vector<int> v{-1, 2, 3};
    ASSERT_ANY_THROW(GetProductsAmount(v, 10));
}

TEST(GetProductsAmountTests, HandleSumOverflow)
{
    std::vector<int> v{1, IntLim::max()};
    ASSERT_NE(v.size(), GetProductsAmount(v, 10));
}

/*
 * WARNING: long-time (near a 30m test),
 * can probably crash on weak computers with out of memory error.
 * Uncomment this if you brave enough
*/
/*
TEST(GetProductsAmountTests, HandleBigVectorSize_CounterOverFlow)
{
    using vtype = std::vector<int>::size_type;
    std::vector <int> v;
    auto size = static_cast<vtype>(IntLim::max()) + 1;
    v.reserve(size);
    v.resize(size, 1);
    ASSERT_EQ(IntLim::max(), GetProductsAmount(v, IntLim::max()));
}
*/
