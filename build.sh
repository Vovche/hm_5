#!/bin/bash

BUILD_DIR=_build

if [ ! -d $BUILD_DIR ];
then
    mkdir $BUILD_DIR
fi;
cd $BUILD_DIR
cmake .. -DCMAKE_BUILD_TYPE=Release
make
ctest -V
